window.onload=function(){
// Enables the game to start via play button
document.getElementById('play').addEventListener('click', initialise);

// Begins the game
function initialise() {

  // preliminary reset to prevent duplicate games
  reset();

  // Reset values
  var
    originalText =
    "<p>" 
	+ " Welcome/Willkommen! <br>" 
	+ "</p>" 
	+ "<ol>" 
	+ "<li>Choose a difficulty.</li>" 
	+ "<li>You will be presented with a selection of words.</li>" 
	+ "<li>You have four chances to guess the correct word from the selection.</li>" 
	+ "<li>Each attempt will reveal whether any characters of the word align with the correct word." 
	+ "</li>" 
	+ "<br> For example: if the correct word is 'HOME' and you guessed 'HOLY' then the response will be '2' " 
	+ " because the words share the characters 'HO' at the same position. If you were to guess" 
	+ " 'DOME' " 
	+ "then the response " 
	+ "will be '2' because both share" 
	+ " 'OM'. " 
	+ " </ol>" 
	+ "<p>" 
	+ " Press the 'Play' button to begin the game." 
	+ "</p>";


  // Store the page's text to later restore
  localStorage.restoreText = originalText;

  // Replace the text
  document.getElementById('prompt').innerHTML = '<p>Choose your preferred difficulty level.</p>';

  // Displays the difficulty form
  document.getElementById('list').style.display = "block";

}

// Enables beginGame function
document.getElementById('submit').addEventListener('click', beginGame);

// Uses chosen difficulty to start the game
function beginGame() {

  // Determine the chosen difficulty
  var listOfButtons = document.getElementById('listContainer'),
    difficulty;

  // Form allows iteration to find checked radio
  for (var x = 0; x < 5; x++) {

    if (listOfButtons.elements[x].type == 'radio') {
      if (listOfButtons.elements[x].checked) {
        difficulty = listOfButtons.elements[x].value;
      }

    }

  }


  // Uses the found radio value to determine length/no of words
  var
    numberOfWords;

  switch (difficulty) {

    case '1':
      numberOfWords = 5;
      break;

    case '2':
      numberOfWords = 7;
      break;

    case '3':
      numberOfWords = 10;
      break;

    case '4':
      numberOfWords = 12;
      break;

    case '5':
      numberOfWords = 15;
      break;

  }

  // Create the list of random words to be given to the user
  var listOfWords = [];

  for (var i = 0; i < numberOfWords; i++) {
    listOfWords.push(chance.word({
      length: numberOfWords
    }));
  }

  // Hide the difficulty menu
  document.getElementById('list').style.display = 'none';
  document.getElementById('words').style.display = 'block';


  // Update the prompt
  document.getElementById('prompt').innerHTML = "<p>Here are your list of words:</p>";

  // Append the list of words
  document.getElementById('words').innerHTML = "<ol>";

  listOfWords.forEach(function(word) {
    document.getElementById('words').innerHTML += "<li><p>" + word + "</li></p>";
  });


  document.getElementById('words').innerHTML += "</ol>";

  userInput(listOfWords);

}

// Enables the guessing aspect of the game
function userInput(list) {

  var guesses = document.getElementById('guess1'),
    correctWordFromList,
    correctWordAsArray,
    guessAsArray,
    guess,
    chance2 = document.getElementById('guess2'),
    chance3 = document.getElementById('guess3'),
    chance4 = document.getElementById('guess4'),
    numberOfCorrectChars = 0,

    // Random word in the list will be the correct word to guess
    indexForRandomWord = Math.floor((Math.random() * list.length) + 0);

  // Shows the input box
  document.getElementById('inputBox').style.display = 'block';
  document.getElementById('guesses').style.display = 'block';

  // Determine the correct word from the input list
  correctWordFromList = list[indexForRandomWord];
  console.log(correctWordFromList);

  // Split the correct word into a char array to check individual chars
  correctWordAsArray = correctWordFromList.split('');

  // Allow input until guesses are used up
  if (guesses.checked) {

    document.getElementById('playerGuess').onclick = function() {
      guess = document.getElementById('playerText').value;

      // check if the guess is valid
      if (guessCheck(correctWordFromList)) {

        // a correct guess ends the game
        if (guess == correctWordFromList) {

          // reset all variables to enable replay
          document.getElementById('prompt').innerHTML = "<p> Congratulations! You won the game.</p>";
          document.getElementById('words').style.display = "none";
          document.getElementById('guesses').style.display = "none";
          document.getElementById('inputBox').style.display = "none";
          document.getElementById('playerText').value = "";

        } else {

          // an incorrect guess shows how many characters were correct
          guessAsArray = document.getElementById('playerText').value.split('');

          for (var i = 0; i < guessAsArray.length; i++) {

            if (guessAsArray[i] == correctWordAsArray[i]) {

              numberOfCorrectChars++;

            }
          }

          document.getElementById('prompt').innerHTML += "<p>Incorrect. Your guess has " + numberOfCorrectChars + "/" + correctWordAsArray.length + " correct characters.</p>";


          // reset the counter
          numberOfCorrectChars = 0;

          // remove a chance for each incorrect answer
          if (chance4.checked) {
            chance4.checked = false;


          } else if (chance3.checked) {
            chance3.checked = false;
          } else if (chance2.checked) {
            chance2.checked = false;
          } else if (guesses.checked) {

            // No more chances so the game ends
            document.getElementById('prompt').innerHTML = "<p> Sorry but you have lost the game due to running out of chances."
			+ "Please press the 'Reset' button if you need a reminder of the rules, else press the 'Play' button.</p>";
            document.getElementById('words').style.display = "none";
            document.getElementById('guesses').style.display = "none";
            document.getElementById('inputBox').style.display = "none";
            document.getElementById('playerText').value = "";
          }
        }

      }

    }

  }

}

// Checks the player's input/guess
function guessCheck(correctWord) {

  var guess = document.getElementById('playerText').value;

  // checks the length of both words
  if (guess.length < correctWord.length) {
    picoModal('You entered a word with an incorrect length. Please resubmit a compatible answer.').show();
    return false;

  } else {
    return true;
  }
}

// Enables the reset button
document.getElementById('reset').addEventListener('click', reset);

// Resets the game
function reset() {

  // restore original text and hide unused divs
  document.getElementById('prompt').innerHTML = localStorage.restoreText;
  document.getElementById('list').style.display = 'none';
  document.getElementById('words').style.display = 'none';
  document.getElementById('inputBox').style.display = 'none';
  document.getElementById('guesses').style.display = 'none';

  // restore guesses for new game
  document.getElementById('guess1').checked = true;
  document.getElementById('guess2').checked = true;
  document.getElementById('guess3').checked = true;
  document.getElementById('guess4').checked = true;

}

// Enables the credit button
document.getElementById('credit').addEventListener('click', displayCredit);

// Displays the dependencies of the project
function displayCredit() {

  picoModal('<b>Libraries used:</b> <br> picoModal - https://github.com/Nycto/PicoModal <br> Buttons - http://unicorn-ui.com/buttons/builder/'
  + '<br> Chance - chancejs.com <br> <b>Fonts used:</b> <br> Header - Pacifico - https://fonts.google.com/specimen/Pacifico'
  + '<br> Body - Open Sans - https://fonts.google.com/specimen/Open+Sans').show();


}

}