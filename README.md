# js-mastermind
A simple rendition of a 'Mastermind'-like game built in JavaScript. Uses a few libraries to aid in decoration. Logic is pure JS.

Simply open 'mastermind.html' in your favourite browser and it should all work. Instructions for the game are contained within the index page.

Originally developed (mostly) in JSFiddle (slightly outdated version):
https://jsfiddle.net/rainygold/w9Lzu3Lt/

Libraries used:
Chance - chancejs.com
Buttons - unicorn-ui.com/buttons/builder/
PicoModal - github.com/Nycto/PicoModal
